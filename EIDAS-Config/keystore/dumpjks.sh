#!/bin/bash

cat > README.md <<EOF
# Dumps of keystores

EOF

for ks in connector.jks connector-metadata.jks demo-idp.jks demo-idp-metadata.jks demo-sp.jks demo-sp-metadata.jks proxy-service.jks proxy-service-metadata.jks; do
    dump=$(keytool -keystore ${ks} -storepass local-demo -list -v 2>/dev/null)
    cat >> README.md <<EOF
## ${ks}

\`\`\`
${dump}
\`\`\`

EOF
done
