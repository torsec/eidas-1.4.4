VOL_NAME := "eidas-144-mvn-repo"
VOL_NAME_EXISTS := $(shell docker volume ls | grep $(VOL_NAME) | wc -l)
CURRENT_DIR := $(shell pwd)

default: run

mvn-repo:
ifeq ($(VOL_NAME_EXISTS),0)
	$(info Creating volume $(VOL_NAME))
	docker volume create --name $(VOL_NAME)
else
	$(info Volume $(VOL_NAME) already defined)
endif

build-wars: mvn-repo
	docker run -ti --rm \
		-v $(VOL_NAME):/root/.m2 \
		-v "$(CURRENT_DIR):/sources" \
		-w /sources/EIDAS-Parent \
		maven:3-jdk-8-alpine \
		mvn clean install -P tomcat

build-devel: build-wars
	docker build --tag torsec/eidas-devel:1.4.4 -f Dockerfile.devel .

run-devel: build-devel
	docker run -ti --rm -p "8888:8080" torsec/eidas-devel:1.4.4

build:
	docker build --tag torsec/eidas:1.4.4 .

run: build
	docker run -ti --rm -p "8888:8080" torsec/eidas:1.4.4
