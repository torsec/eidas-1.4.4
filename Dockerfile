FROM maven:3-jdk-8-alpine AS builder

LABEL maintainer="Paolo Smiraglia" \
      maintainer.email="paolo.smiraglia@gmail.com"

WORKDIR /sources
COPY . .

WORKDIR EIDAS-Parent
ENV MAVEN_OPTS="-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn"
RUN mvn clean install -P tomcat

FROM tomcat:8-jre8-alpine

LABEL maintainer="Paolo Smiraglia" \
      maintainer.email="paolo.smiraglia@gmail.com"

COPY ./AdditionalFiles/endorsed/ /usr/local/tomcat/lib/
RUN mv /usr/local/tomcat/lib/serializer-2.7.2.jar /usr/local/tomcat/lib/serializer.jar

RUN mkdir -p \
    /opt/eidas/conf \
    /opt/eidas/keystore \
    /opt/eidas/logs

COPY ./EIDAS-Config/server/ /opt/eidas/conf/
COPY ./EIDAS-Config/keystore/ /opt/eidas/keystore/

RUN { \
        echo 'JAVA_OPTS="${JAVA_OPTS} -DLOG_HOME=/opt/eidas/logs"'; \
        echo 'JAVA_OPTS="${JAVA_OPTS} -DEIDAS_CONFIG_REPOSITORY=/opt/eidas/conf/"'; \
        echo 'JAVA_OPTS="${JAVA_OPTS} -DSPECIFIC_CONFIG_REPOSITORY=/opt/eidas/conf/specific/"'; \
        echo 'JAVA_OPTS="${JAVA_OPTS} -DSP_CONFIG_REPOSITORY=/opt/eidas/conf/sp/"'; \
        echo 'JAVA_OPTS="${JAVA_OPTS} -DIDP_CONFIG_REPOSITORY=/opt/eidas/conf/idp/"'; \
    } > ${CATALINA_HOME}/bin/setenv.sh \
    && chmod +x ${CATALINA_HOME}/bin/setenv.sh

COPY --from=builder /sources/EIDAS-SP/target/SP.war /usr/local/tomcat/webapps/
COPY --from=builder /sources/EIDAS-IdP-1.0/target/IdP.war /usr/local/tomcat/webapps/
COPY --from=builder /sources/EIDAS-Node/target/EidasNode.war /usr/local/tomcat/webapps/

# vim: ft=dockerfile
