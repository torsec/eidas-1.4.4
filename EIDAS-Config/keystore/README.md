# Dumps of keystores

## connector.jks

```
Keystore type: jks
Keystore provider: SUN

Your keystore contains 3 entries

Alias name: proxy-service-metadata
Creation date: Jan 11, 2019
Entry type: trustedCertEntry

Owner: CN=proxy service metadata, C=CA
Issuer: CN=proxy service metadata, C=CA
Serial number: 3a990529
Valid from: Fri Jan 11 10:34:22 CET 2019 until: Mon Jan 08 10:34:22 CET 2029
Certificate fingerprints:
	 MD5:  3F:8D:3C:AB:F6:75:4B:96:89:02:93:E7:CD:AD:95:EE
	 SHA1: E6:18:17:5E:87:C7:D5:F8:5A:77:6D:E3:C7:92:02:69:1C:47:F5:9D
	 SHA256: 3C:0D:2F:94:F6:B1:8F:27:05:9B:5D:DD:F6:AA:B7:45:B4:D3:2D:54:45:52:F0:50:DF:68:25:48:B8:2E:55:F0
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 53 BF 7E FA C0 1D E4 26   FC 95 19 84 C8 27 95 6F  S......&.....'.o
0010: 37 52 0B C1                                        7R..
]
]



*******************************************
*******************************************


Alias name: demo-sp-metadata
Creation date: Jan 11, 2019
Entry type: trustedCertEntry

Owner: CN=demo sp metadata, C=CA
Issuer: CN=demo sp metadata, C=CA
Serial number: 66205048
Valid from: Thu Jan 10 21:35:36 CET 2019 until: Sun Jan 07 21:35:36 CET 2029
Certificate fingerprints:
	 MD5:  C7:CB:C9:D3:BB:75:3F:30:8E:2D:8C:7F:12:0D:0A:8B
	 SHA1: FD:91:69:FC:3A:49:2D:C3:80:44:64:BB:2C:6C:94:BB:3D:48:0C:EF
	 SHA256: 07:63:63:61:AD:99:43:32:F5:46:97:DC:15:A1:2B:6C:DB:19:20:5C:B7:DA:F9:6F:9E:4A:12:49:BB:25:0D:EA
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 4E 05 FF 0B 2D 3B E5 6B   D1 D1 E4 D7 CB 69 FA D5  N...-;.k.....i..
0010: EA 19 CF 6A                                        ...j
]
]



*******************************************
*******************************************


Alias name: mykey
Creation date: Jan 11, 2019
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=connector, C=CA
Issuer: CN=connector, C=CA
Serial number: 45e9eb9e
Valid from: Fri Jan 11 10:34:59 CET 2019 until: Mon Jan 08 10:34:59 CET 2029
Certificate fingerprints:
	 MD5:  60:9D:0A:83:59:01:94:EE:2B:A3:D1:E7:D8:2E:96:4F
	 SHA1: 7B:BE:F9:E1:B9:8C:7B:E5:06:9E:BF:3D:CB:05:0B:81:12:00:9B:C6
	 SHA256: AB:B9:57:17:73:1C:CC:44:E7:8F:D9:65:25:BF:C9:A7:40:DF:3E:28:A1:46:2E:04:63:2C:0C:AF:E6:54:3F:82
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 78 7E 48 8B DC 80 86 14   6E AC B4 BB EA 36 F5 E0  x.H.....n....6..
0010: 6C 4D DD 18                                        lM..
]
]



*******************************************
*******************************************
```

## connector-metadata.jks

```
Keystore type: jks
Keystore provider: SUN

Your keystore contains 1 entry

Alias name: mykey
Creation date: Jan 11, 2019
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=connector metadata, C=CA
Issuer: CN=connector metadata, C=CA
Serial number: 480dbb81
Valid from: Fri Jan 11 10:34:48 CET 2019 until: Mon Jan 08 10:34:48 CET 2029
Certificate fingerprints:
	 MD5:  62:70:90:46:3B:2F:94:5B:3E:5C:7F:06:74:39:3B:6A
	 SHA1: 38:3A:BC:07:FA:ED:41:6A:0C:42:13:30:A4:B9:6E:9E:7E:68:77:32
	 SHA256: 14:24:6F:0A:11:D8:F3:8E:77:94:3A:98:56:AC:F4:B0:AB:14:2F:7C:80:2C:36:D3:16:6D:72:1E:64:D3:31:76
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 60 35 BE 9A DC 8C 2A 64   3B 0C 2E C6 C6 B7 5D 10  `5....*d;.....].
0010: FB BD 8E 32                                        ...2
]
]



*******************************************
*******************************************
```

## demo-idp.jks

```
Keystore type: jks
Keystore provider: SUN

Your keystore contains 2 entries

Alias name: proxy-service-metadata
Creation date: Jan 11, 2019
Entry type: trustedCertEntry

Owner: CN=proxy service metadata, C=CA
Issuer: CN=proxy service metadata, C=CA
Serial number: 3a990529
Valid from: Fri Jan 11 10:34:22 CET 2019 until: Mon Jan 08 10:34:22 CET 2029
Certificate fingerprints:
	 MD5:  3F:8D:3C:AB:F6:75:4B:96:89:02:93:E7:CD:AD:95:EE
	 SHA1: E6:18:17:5E:87:C7:D5:F8:5A:77:6D:E3:C7:92:02:69:1C:47:F5:9D
	 SHA256: 3C:0D:2F:94:F6:B1:8F:27:05:9B:5D:DD:F6:AA:B7:45:B4:D3:2D:54:45:52:F0:50:DF:68:25:48:B8:2E:55:F0
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 53 BF 7E FA C0 1D E4 26   FC 95 19 84 C8 27 95 6F  S......&.....'.o
0010: 37 52 0B C1                                        7R..
]
]



*******************************************
*******************************************


Alias name: mykey
Creation date: Jan 10, 2019
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=demo idp, C=CA
Issuer: CN=demo idp, C=CA
Serial number: 128de9e0
Valid from: Thu Jan 10 21:35:54 CET 2019 until: Sun Jan 07 21:35:54 CET 2029
Certificate fingerprints:
	 MD5:  63:2A:7B:C6:1D:7A:F6:7C:19:D1:56:AD:84:8A:39:74
	 SHA1: C8:66:86:60:C2:83:34:74:32:07:A2:6E:A9:37:16:CB:74:76:01:45
	 SHA256: E5:32:9A:51:67:7D:1F:C2:21:73:5E:4B:72:3D:60:AF:DE:14:2C:C5:F4:C6:40:5F:3A:4B:BB:9C:F0:3D:8C:7A
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 01 FC 12 D9 B7 C9 FC 89   09 37 0B 0F B9 6D 9D 95  .........7...m..
0010: 92 3E EF 93                                        .>..
]
]



*******************************************
*******************************************
```

## demo-idp-metadata.jks

```
Keystore type: jks
Keystore provider: SUN

Your keystore contains 1 entry

Alias name: mykey
Creation date: Jan 10, 2019
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=demo idp metadata, C=CA
Issuer: CN=demo idp metadata, C=CA
Serial number: 57bf19d7
Valid from: Thu Jan 10 21:36:06 CET 2019 until: Sun Jan 07 21:36:06 CET 2029
Certificate fingerprints:
	 MD5:  AB:47:57:53:79:AC:2F:3F:D2:F8:6C:3C:57:04:51:E9
	 SHA1: BC:34:34:C2:45:1B:FA:35:80:68:B9:37:C4:8C:8F:99:60:D6:A7:70
	 SHA256: 6D:52:6F:F5:94:5D:57:38:6A:1A:A0:69:57:2C:3E:5B:78:99:44:49:E5:07:B4:97:5C:4F:88:46:74:CB:54:3A
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 6A 8A CF 65 C1 50 19 00   DA 56 00 CD D2 4F 98 6B  j..e.P...V...O.k
0010: F3 6F B3 F7                                        .o..
]
]



*******************************************
*******************************************
```

## demo-sp.jks

```
Keystore type: jks
Keystore provider: SUN

Your keystore contains 2 entries

Alias name: connector-metadata
Creation date: Jan 11, 2019
Entry type: trustedCertEntry

Owner: CN=connector metadata, C=CA
Issuer: CN=connector metadata, C=CA
Serial number: 480dbb81
Valid from: Fri Jan 11 10:34:48 CET 2019 until: Mon Jan 08 10:34:48 CET 2029
Certificate fingerprints:
	 MD5:  62:70:90:46:3B:2F:94:5B:3E:5C:7F:06:74:39:3B:6A
	 SHA1: 38:3A:BC:07:FA:ED:41:6A:0C:42:13:30:A4:B9:6E:9E:7E:68:77:32
	 SHA256: 14:24:6F:0A:11:D8:F3:8E:77:94:3A:98:56:AC:F4:B0:AB:14:2F:7C:80:2C:36:D3:16:6D:72:1E:64:D3:31:76
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 60 35 BE 9A DC 8C 2A 64   3B 0C 2E C6 C6 B7 5D 10  `5....*d;.....].
0010: FB BD 8E 32                                        ...2
]
]



*******************************************
*******************************************


Alias name: mykey
Creation date: Jan 10, 2019
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=demo sp, C=CA
Issuer: CN=demo sp, C=CA
Serial number: 3ec182f
Valid from: Thu Jan 10 21:33:50 CET 2019 until: Sun Jan 07 21:33:50 CET 2029
Certificate fingerprints:
	 MD5:  24:F7:D6:09:21:F8:73:7D:10:BD:D0:15:95:76:A3:7D
	 SHA1: E0:48:BD:A0:10:07:3F:44:04:D4:09:64:43:75:33:37:3F:6A:3D:0E
	 SHA256: 3D:95:54:70:40:85:C6:1C:55:7A:5E:AB:80:16:A3:64:FB:91:BB:A1:3F:60:4F:E4:07:95:3D:96:CA:BC:FA:B3
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: FC 55 3C 1F 40 EE EA 83   5A A3 10 D7 23 67 30 96  .U<.@...Z...#g0.
0010: 25 E5 BB 61                                        %..a
]
]



*******************************************
*******************************************
```

## demo-sp-metadata.jks

```
Keystore type: jks
Keystore provider: SUN

Your keystore contains 1 entry

Alias name: mykey
Creation date: Jan 10, 2019
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=demo sp metadata, C=CA
Issuer: CN=demo sp metadata, C=CA
Serial number: 66205048
Valid from: Thu Jan 10 21:35:36 CET 2019 until: Sun Jan 07 21:35:36 CET 2029
Certificate fingerprints:
	 MD5:  C7:CB:C9:D3:BB:75:3F:30:8E:2D:8C:7F:12:0D:0A:8B
	 SHA1: FD:91:69:FC:3A:49:2D:C3:80:44:64:BB:2C:6C:94:BB:3D:48:0C:EF
	 SHA256: 07:63:63:61:AD:99:43:32:F5:46:97:DC:15:A1:2B:6C:DB:19:20:5C:B7:DA:F9:6F:9E:4A:12:49:BB:25:0D:EA
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 4E 05 FF 0B 2D 3B E5 6B   D1 D1 E4 D7 CB 69 FA D5  N...-;.k.....i..
0010: EA 19 CF 6A                                        ...j
]
]



*******************************************
*******************************************
```

## proxy-service.jks

```
Keystore type: jks
Keystore provider: SUN

Your keystore contains 3 entries

Alias name: demo-idp-metadata
Creation date: Jan 11, 2019
Entry type: trustedCertEntry

Owner: CN=demo idp metadata, C=CA
Issuer: CN=demo idp metadata, C=CA
Serial number: 57bf19d7
Valid from: Thu Jan 10 21:36:06 CET 2019 until: Sun Jan 07 21:36:06 CET 2029
Certificate fingerprints:
	 MD5:  AB:47:57:53:79:AC:2F:3F:D2:F8:6C:3C:57:04:51:E9
	 SHA1: BC:34:34:C2:45:1B:FA:35:80:68:B9:37:C4:8C:8F:99:60:D6:A7:70
	 SHA256: 6D:52:6F:F5:94:5D:57:38:6A:1A:A0:69:57:2C:3E:5B:78:99:44:49:E5:07:B4:97:5C:4F:88:46:74:CB:54:3A
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 6A 8A CF 65 C1 50 19 00   DA 56 00 CD D2 4F 98 6B  j..e.P...V...O.k
0010: F3 6F B3 F7                                        .o..
]
]



*******************************************
*******************************************


Alias name: connector-metadata
Creation date: Jan 11, 2019
Entry type: trustedCertEntry

Owner: CN=connector metadata, C=CA
Issuer: CN=connector metadata, C=CA
Serial number: 480dbb81
Valid from: Fri Jan 11 10:34:48 CET 2019 until: Mon Jan 08 10:34:48 CET 2029
Certificate fingerprints:
	 MD5:  62:70:90:46:3B:2F:94:5B:3E:5C:7F:06:74:39:3B:6A
	 SHA1: 38:3A:BC:07:FA:ED:41:6A:0C:42:13:30:A4:B9:6E:9E:7E:68:77:32
	 SHA256: 14:24:6F:0A:11:D8:F3:8E:77:94:3A:98:56:AC:F4:B0:AB:14:2F:7C:80:2C:36:D3:16:6D:72:1E:64:D3:31:76
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 60 35 BE 9A DC 8C 2A 64   3B 0C 2E C6 C6 B7 5D 10  `5....*d;.....].
0010: FB BD 8E 32                                        ...2
]
]



*******************************************
*******************************************


Alias name: mykey
Creation date: Jan 11, 2019
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=proxy service, C=CA
Issuer: CN=proxy service, C=CA
Serial number: 69eba4c0
Valid from: Fri Jan 11 10:33:57 CET 2019 until: Mon Jan 08 10:33:57 CET 2029
Certificate fingerprints:
	 MD5:  AB:B6:F8:77:E2:11:D6:C7:FE:1B:B8:66:ED:7C:44:0C
	 SHA1: D1:16:53:47:E3:D0:A5:44:A5:F5:1C:5A:62:73:2A:71:42:CC:5F:EE
	 SHA256: C1:F0:08:95:6E:DC:6A:E9:0A:D0:C6:89:B8:F9:F2:E1:D9:D3:08:63:7D:64:D6:71:70:3A:A4:21:8E:2A:A4:5B
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 6A A2 BC 16 73 F2 F3 46   87 B0 39 B2 2C 7F 8F B6  j...s..F..9.,...
0010: 55 09 AD 99                                        U...
]
]



*******************************************
*******************************************
```

## proxy-service-metadata.jks

```
Keystore type: jks
Keystore provider: SUN

Your keystore contains 1 entry

Alias name: mykey
Creation date: Jan 11, 2019
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=proxy service metadata, C=CA
Issuer: CN=proxy service metadata, C=CA
Serial number: 3a990529
Valid from: Fri Jan 11 10:34:22 CET 2019 until: Mon Jan 08 10:34:22 CET 2029
Certificate fingerprints:
	 MD5:  3F:8D:3C:AB:F6:75:4B:96:89:02:93:E7:CD:AD:95:EE
	 SHA1: E6:18:17:5E:87:C7:D5:F8:5A:77:6D:E3:C7:92:02:69:1C:47:F5:9D
	 SHA256: 3C:0D:2F:94:F6:B1:8F:27:05:9B:5D:DD:F6:AA:B7:45:B4:D3:2D:54:45:52:F0:50:DF:68:25:48:B8:2E:55:F0
Signature algorithm name: SHA512withRSA
Subject Public Key Algorithm: 3072-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 53 BF 7E FA C0 1D E4 26   FC 95 19 84 C8 27 95 6F  S......&.....'.o
0010: 37 52 0B C1                                        7R..
]
]



*******************************************
*******************************************
```

